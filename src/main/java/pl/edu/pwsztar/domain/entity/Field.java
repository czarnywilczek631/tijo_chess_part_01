package pl.edu.pwsztar.domain.entity;

public class Field {
    private int x, y;

    public Field(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }
}
