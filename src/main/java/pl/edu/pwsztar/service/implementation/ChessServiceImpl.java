package pl.edu.pwsztar.service.implementation;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.entity.Field;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

public class ChessServiceImpl implements ChessService {

    private FigureMoveDto figureMoveDto;
    public ChessServiceImpl(FigureMoveDto figureMoveDto){
        this.figureMoveDto = figureMoveDto;
    }

    Field convert(String string){
        String str = "abcdefgh";
        int x = str.indexOf(string.charAt(0));
        int y = Integer.parseInt(String.valueOf(string.charAt(2)));
        return new Field(x, y);
    }

    public boolean checkMove(){
        Field start = convert(figureMoveDto.getSource());
        Field end = convert(figureMoveDto.getDestination());
        if (figureMoveDto.getType().equals(FigureType.BISHOP)){
            int diffX = start.getX() - end.getX();
            int diffY = start.getY() - end.getY();
            if (diffX == 0 || diffY == 0) return false;
            diffX = diffX > 0 ? diffX : -diffX;
            diffY = diffY > 0 ? diffY : -diffY;
            if (diffX == diffY) return true;
            return false;
        };
        return false;
    }
}
